# Zoology
A small program that allows the user to create a lion or a sheep, and then returns information about the animal.


## Getting Started
Open the folder in Visual studio, and run the program.cs file there.


## Author
* **Ådne Skeie** - [adnske7](https://gitlab.com/adnske7)
