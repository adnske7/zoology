﻿using System;
using System.Collections;

namespace Ctask6
{
    class MainClass
    {
        public class Animal
        {
            public string name;
            public Boolean EatsMeat = false;
            

            public void PrintBase()
            {
                Console.WriteLine("Name: " + name);
                Console.WriteLine("Does eat meat: " + EatsMeat);
            }


        }

        public class Lion : Animal
        {
            public int scaryness;

            public void ifEscaped()
            {
                Console.WriteLine("RUN!!!!");
            }
        }
        public class Sheep : Animal
        {
            public int weight;


            public void wutAnimal()
            {
                Console.WriteLine("smol sheep");
            }



        }

        public static Lion CreateLion()
        {
            Lion newlion = new Lion();
            Console.WriteLine("What would you like to name your lion?");
            newlion.name = Console.ReadLine();
            newlion.scaryness = 5;
            newlion.EatsMeat = true;
            newlion.PrintBase();
            Console.WriteLine(newlion.name +" has a scarynessscore of " + newlion.scaryness + "/10");
            Console.WriteLine("If " + newlion.name + " escapes: ");
            newlion.ifEscaped();
            return newlion;
        }

        public static Sheep CreateSheep()
        {
            Sheep newsheep = new Sheep();
            Console.WriteLine("What would you like to name you sheep?");
            newsheep.name = Console.ReadLine();
            newsheep.weight = 80;
            newsheep.PrintBase();
            Console.WriteLine(newsheep.name + " weighs: " + newsheep.weight + " kg");
            newsheep.wutAnimal();
            return newsheep;
        }


        public static void Main(string[] args)
        {
            ArrayList myAnimals = new ArrayList();
            Console.WriteLine("Would you like to create a lion or a sheep");
            string input = Console.ReadLine().ToLower();

            switch (input)
            {
                case "lion":
                    Lion lion = CreateLion();
                    myAnimals.Add(lion.name);

                    break;

                case "sheep":
                    Sheep sheep = CreateSheep();
                    myAnimals.Add(sheep.name);
                    break;
            }
            



           

        }

    }
}


